---?image=https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg&position=5% 50%&size=20%&color=orange

## @color[black](Markdown)
### Mardown for daily usage
#### Alexandre ZANNI (noraj)

---

## What is it?

+ text-to-HTML conversion tool
+ plain text format
+ easy-to-read / easy-to-write
+ created by John Gruber (of [Daring Fireball](https://daringfireball.net/projects/markdown/))

---

## Why should I use it?

+ write fast
+ fast formating
+ no mouse required
+ no GUI required
+ readable even not rendered

---

## Open an editor with previsualisation:

+ Visual Studio Code
+ Atom
+ [https://stackedit.io/editor](https://stackedit.io/editor)

---

## Markdown syntax

---

### Paragraphs and line breaks

This is a paragraph. And a second sentence.

```markdown
This is a paragraph. And a second sentence.
```

This is a paragraph.
And a second sentence.

```markdown
This is a paragraph.
And a second sentence.
```

This is a paragraph.

This is a second paragraph.

```markdown
This is a paragraph.

This is a second paragraph.
```

---


### Headers

# H1 title

```markdown
# H1 title
```

## H2 title

```markdown
## H2 title
```

###### H6 title

```markdown
###### H6 title
```

---

### Emphasis

*single asterisks*

_single underscores_

**double asterisks**

__double underscores__

```markdown
*single asterisks*

_single underscores_

**double asterisks**

__double underscores__
```

---

### Lists

+++

#### Ordered (numbered)

1. First item
2. Second item
3. Third item

```markdown
1. First item
2. Second item
3. Third item
```

+++

#### Ordered (numbered)

7. First item
1. Second item
5. Third item

```markdown
7. First item
1. Second item
5. Third item
```

+++

####  Unordered (bulleted)

- Item
- Item
  - Item
  - Item
- Item

```markdown
- Item
- Item
  - Item
  - Item
- Item
```

+++

#### Unordered (bulleted)

- Item
+ Item
* Item

```markdown
- Item
+ Item
* Item
```

---

### Links

[inline-style link](https://www.archlinux.org/)

```markdown
[inline-style link](https://www.archlinux.org/)
```

[link with title](https://www.archlinux.org/ "ArchLinux's Homepage")

```markdown
[link with title](https://www.archlinux.org/ "ArchLinux's Homepage")
```

[relative link](./folder/file)

```markdown
[relative link](./folder/file)
```

+++

### Links

[reference-style link][ref]

[ref]:https://www.archlinux.org/

```markdown
[reference-style link][ref]

[ref]:https://www.archlinux.org/
```

[reference-style link with title][2]

[2]:https://www.archlinux.org/ "ArchLinux's Site"

```markdown
[reference-style link with title][2]

[2]:https://www.archlinux.org/ "ArchLinux's Site"
```

---

### Images

![alt text](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

```markdown
![alt text](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)
```

+++

### Images

![alt text][ArchLinux]

[ArchLinux]:https://upload.wikimedia.org/wikipedia/commons/7/74/Arch_Linux_logo.svg

```markdown
![alt text][ArchLinux]

[ArchLinux]:https://upload.wikimedia.org/wikipedia/commons/7/74/Arch_Linux_logo.svg
```

---

### Blocksquotes

> Quote. Quote. Quote.
> Next. Next. Next.
>
> Source

```markdown
> Quote. Quote. Quote.
> Next. Next. Next.
>
> Source
```

+++

### Blocksquotes

> This is the first level of quoting.
>
> > This is nested blockquote.
>
> Back to the first level.

```markdown
> This is the first level of quoting.
>
> > This is nested blockquote.
>
> Back to the first level.
```

+++

### Blocksquotes

> #### This is a header.
>
> 1.   This is the first list item.
> 2.   This is the second list item.
>
> Here's some example code:
>
>     return shell_exec("echo $input | $markdown_script");

```markdown
> #### This is a header.
>
> 1.   This is the first list item.
> 2.   This is the second list item.
>
> Here's some example code:
>
>     return shell_exec("echo $input | $markdown_script");
```

---

### Baclslash escapes

```text
\   backslash
`   backtick
*   asterisk
_   underscore
{}  curly braces
[]  square brackets
()  parentheses
#   hash mark
+   plus sign
-   minus sign (hyphen)
.   dot
!   exclamation mark
```

---

### Horizontal rule

Three or more...

+ Hyphens `---`
+ Asterisks `***`
+ Underscores `___`

___

---

## GitHub Flavored Markdown

### **GFM**: GitHub Flavored Markdown

---

## Differences from traditional Markdown

+++

### Multiple underscores in words

+ Markdown: transforms `_` into italics in words
+ `wow_great_stuff` or `my_long_var_name` ?
+ render properly, use `*` for italic in words

+++

### Strikethrough

Strikethrough support:

~~This is an error~~

```markdown
~~This is an error~~
```

+++

###  Fenced code blocks

Inline: `` `backtick` `` => `backtick`

4 spaces:

    code line 1
    line 2

```markdown
    code line 1
    line 2
```

Fenced block:

```
code line 1
line 2
```

~~~
```
code line 1
line 2
```
~~~

+++

### Syntax highlighting

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

+ Ruby:
    - [rouge](http://rouge.jneen.net/)
    - [linguist](https://github.com/github/linguist)
+ JavaScript: [highlight.js](https://highlightjs.org/)
+ Python: [pygments](http://pygments.org/)

Support of languages may vary.

+++

### Tables

First Header  | Second Header
---           | ---
Content Cell  | Content Cell
Content Cell  | Content Cell

```markdown
First Header  | Second Header
---           | ---
Content Cell  | Content Cell
Content Cell  | Content Cell
```

```markdown
| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |
```

+++

### Tables

Name | Description |
--- | ---
Help      | ~~Display the~~ help window.
Close     | _Closes_ a window

```markdown
Name | Description |
--- | ---
Help      | ~~Display the~~ help window.
Close     | _Closes_ a window
```

+++

### Tables

| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ |:---------------:| -----:|
| col 3 is      | some wordy text | $1600 |
| col 2 is      | centered        |   $12 |
| zebra stripes | are neat        |    $1 |

```markdown
| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ |:---------------:| -----:|
| col 3 is      | some wordy text | $1600 |
| col 2 is      | centered        |   $12 |
| zebra stripes | are neat        |    $1 |
```

---

## Who use Markdown?

[Reddit][1], [Github][2], [Ghost][3], [StackExchange][4], [Hexo][5], [GitLab][6], [DokuWiki][7], [BitBucket][8], [CTFTime][9], [ZenDesk][10], [Reveal.js][11], [Madoko][12], [Pandoc][13], [Marp][14], [Atom][15],  etc...

[1]:https://www.reddit.com/
[2]:https://github.com/
[3]:https://ghost.org/
[4]:http://stackexchange.com/
[5]:https://hexo.io/
[6]:https://gitlab.com/
[7]:https://www.dokuwiki.org/
[8]:https://bitbucket.org/
[9]:https://ctftime.org/
[10]:https://www.zendesk.fr/
[11]:http://lab.hakim.se/reveal-js/
[12]:https://www.madoko.net/
[13]:http://pandoc.org/
[14]:https://yhatt.github.io/marp/
[15]:https://atom.io/


## Other variant

+ [Pandocs markdown](http://pandoc.org/MANUAL.html#pandocs-markdown)
+ [PHP Markdown Extra](https://michelf.ca/projects/php-markdown/extra/)
+ [MultiMarkdown](http://fletcherpenney.net/multimarkdown/)

---

## Limitations

http://www.adamhyde.net/whats-wrong-with-markdown/

---?image=https://pbs.twimg.com/profile_images/730466514223042561/CWykp4WH_400x400.jpg&size=auto&position=5% 50%

## About

Presentation written in **Markdown** by *Alexandre ZANNI* (noraj) with [Reveal.js](http://lab.hakim.se/reveal-js/)/[GitPitch](https://gitpitch.com/).

Presented by *Alexandre ZANNI* at HACK2G2 public conferences the 2016/09/24. Updated the 2019/02/16.

[Twitter](https://twitter.com/hack2g2) / [Facebook](https://fr-fr.facebook.com/hack2g2/)
