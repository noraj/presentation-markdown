A presentation about Markdown.

## About

+ Slideshow: https://gitpitch.com/noraj/presentation-markdown/master?grs=gitlab
+ Offline bundle: https://gitpitch.com/pitchme/offline/gitlab/noraj/presentation-markdown/master/white/PITCHME.zip
+ PDF: https://gitpitch.com/pitchme/print/gitlab/noraj/presentation-markdown/master/white/PITCHME.pdf
+ Source: https://gitlab.com/noraj/presentation-markdown

Presentation written in **Markdown** by *Alexandre ZANNI* (noraj) with [Reveal.js](http://lab.hakim.se/reveal-js/)/[GitPitch](https://gitpitch.com/).

Presented by *Alexandre ZANNI* at HACK2G2 public conferences the 2016/09/24. Updated the 2019/02/16.

![](https://pbs.twimg.com/profile_images/730466514223042561/CWykp4WH_400x400.jpg)

[Twitter](https://twitter.com/hack2g2) / [Facebook](https://fr-fr.facebook.com/hack2g2/)
